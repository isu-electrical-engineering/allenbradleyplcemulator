#define CATCH_CONFIG_RUNNER
#include <gmock/gmock.h>
#include <catch2/catch.hpp>

//
// Created by paulgrahek on 8/21/19.
//

int main(int argc, char **argv) {
    ::testing::GTEST_FLAG(throw_on_failure) = true;
    ::testing::InitGoogleMock(&argc, argv);
    int result = Catch::Session().run(argc,argv);
    return result;
}