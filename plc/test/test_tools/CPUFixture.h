//
// Created by paulp on 9/9/2019.
//

#pragma once
#include "cpu.h"

class CPUFixture {
private:
    std::shared_ptr<CPU> cpu;
public:
    CPUFixture(){
        cpu = std::make_shared<CPU>(false);
    }
    ~CPUFixture(){
        cpu.reset();
    }

    CPU& getCPU(){
        return *cpu;
    }
};