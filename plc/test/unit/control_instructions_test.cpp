//
// Created by paulp on 8/29/2019.
//

#include <catch2/catch.hpp>
#include <plc.h>
#include "CPUFixture.h"

TEST_CASE_METHOD(CPUFixture,"Control Instructions","[unit][instructions]"){
    CPU& cpu = CPUFixture::getCPU();
    SECTION("RNG"){
        cpu.RNG();
        REQUIRE(cpu.getFileNumber() == 0);
        REQUIRE(cpu.getRungNumber() == 1);
        REQUIRE(cpu.getNestLevel() == 0);
        REQUIRE(cpu.getBranchLevel() == 0);
        REQUIRE(cpu.getInstructionNum() == 0);
    }
    SECTION("BR"){
        std::array<bit,256> stack{1,0};
        cpu.setStack(stack);
        cpu.setStackInd(1);
        cpu.setFileNumber(1);
        cpu.setRungNumber(1);
        cpu.setNestLevel(0);
        cpu.setBranchLevel(0);
        cpu.BR();
        REQUIRE(cpu.getFileNumber() == 1);
        REQUIRE(cpu.getRungNumber() == 1);
        REQUIRE(cpu.getNestLevel() == 1);
        REQUIRE(cpu.getBranchLevel() == 0);
    }
    SECTION("BN"){
        std::array<bit,256> stack{1,0,0};
        cpu.setStack(stack);
        cpu.setFileNumber(1);
        cpu.setRungNumber(1);
        cpu.setStackInd(2);
        cpu.setNestLevel(1);
        cpu.setBranchLevel(0);
        cpu.BN();
        REQUIRE(cpu.getFileNumber() == 1);
        REQUIRE(cpu.getRungNumber() == 1);
        REQUIRE(cpu.getNestLevel() == 1);
        REQUIRE(cpu.getBranchLevel() == 1);
        REQUIRE(cpu.getStackInd() == 2);
        REQUIRE(cpu.getStack()[2] == 1);
    }
//    SECTION("BE"){
//        std::array<bit,256> stack{1,0};
//        cpu.setStack(stack);
//        ControlInstruction::BE(cpu);
//    }
}