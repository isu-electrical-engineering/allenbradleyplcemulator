//
// Created by paulgrahek on 8/27/19.
//
#include "catch2/catch.hpp"
#include "CPUFixture.h"



struct BasicInstructionTestCase {
    bit StackValue = false;
    DataAddress Address;
    int AddressValue = 0;
    bit ExpectedValue = false;
};

TEST_CASE_METHOD(CPUFixture,"Basic Instructions","[unit][instructions]"){
    CPU& cpu = CPUFixture::getCPU();
    SECTION("XIC Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,false});
        testCases.push_back({true,addr,0,false});
        testCases.push_back({true,addr,1,true});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.XIC(testCase.Address);
                REQUIRE(cpu.getStackValue(0) == testCase.ExpectedValue);
            }
        }
    }
    SECTION("XIO Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,false});
        testCases.push_back({true,addr,0,true});
        testCases.push_back({true,addr,1,false});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                                            ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.XIO(testCase.Address);
                REQUIRE(cpu.getStackValue(0) == testCase.ExpectedValue);
            }
        }
    }
    SECTION("OTE Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,false});
        testCases.push_back({true,addr,0,true});
        testCases.push_back({true,addr,1,true});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                                            ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.OTE(testCase.Address);
                REQUIRE(cpu.getDataBit(testCase.Address) == testCase.ExpectedValue);
            }
        }
    }
    SECTION("OTL Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,true});
        testCases.push_back({true,addr,0,true});
        testCases.push_back({true,addr,1,true});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                                            ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.OTL(testCase.Address);
                REQUIRE(cpu.getDataBit(testCase.Address) == testCase.ExpectedValue);
            }
        }
    }
    SECTION("OTU Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,false});
        testCases.push_back({true,addr,0,false});
        testCases.push_back({true,addr,1,true});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                                            ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.OTU(testCase.Address);
                REQUIRE(cpu.getDataBit(testCase.Address) == testCase.ExpectedValue);
            }
        }
    }
    SECTION("OSR Instruction"){
        DataAddress addr = DataAddress::parseDataAddressString("I:1/3");
        std::vector<BasicInstructionTestCase> testCases;
        testCases.push_back({false,addr,0,false});
        testCases.push_back({false,addr,1,false});
        testCases.push_back({true,addr,0,true});
        testCases.push_back({true,addr,1,false});
        for(auto testCase : testCases){
            DYNAMIC_SECTION("Stack Value: " << testCase.StackValue <<
                                            ", Data Address Value: " << testCase.AddressValue){
                cpu.setDataBit(testCase.Address,testCase.AddressValue);
                cpu.setStackInd(0);
                cpu.setStackValue(0, testCase.StackValue);
                cpu.OSR(testCase.Address);
                REQUIRE(cpu.getStackValue(0) == testCase.ExpectedValue);
            }
        }
    }
}