list(APPEND testAllenBradleyPLC_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/basic_instructions_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/control_instructions_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/counter_timer_instructions_test.cpp
)