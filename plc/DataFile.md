## Data File Syntax
The syntax used in the data files is meant to closely resemble that of a Data Address for program files
### IO Files
#### Line Syntax
 - [Data Type][File Number]:[Slot Number].[Word Number] [Valid Terminals]
 - Output Files are implicitly 0
 - Input Files are implicitly 1
 - If no word is given, it is implicitly 0
 

#### Examples
Output File 
```
O:1 0b1111111111111111 // Enables all 16 Output Terminals on Slot 1, Word 0 
O:1.1 0b0000000011111111 // Enables first 8 Output Terminals on Slot 1, Word 1
O:2 0b0000000000001111 // Enables first 4 Output Terminals on Slot 2, Word 0 
```

Input File
```
I:1 0b1111111111111111 // Enables all 16 Input Terminals on Slot 1, Word 0 
I:1.1 0b0000000011111111 // Enables first 8 Input Terminals on Slot 1, Word 1
I:2 0b0000000000001111 // Enables first 4 Input Terminals on Slot 2, Word 0
```
