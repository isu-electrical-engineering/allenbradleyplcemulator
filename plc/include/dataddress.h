//
// Created by paulp on 2/25/2019.
//

#pragma once


#include <stdint.h>
#include <string>
#include "types.h"

/**
 * @brief
 *
 * Generic Format for addressing input Xf:e.s/b
 */
struct DataAddress {


    /**
     * @brief data type. Address Alias: X
     */
    DataType type;

    /**
     * @brief file number. Address Alias: f
     */
    int8_t fileNumber = 0;

    /**
     * @brief element number. Address Alias: e
     */
    int64_t elementNumber = 0;

    /**
     * @brief word number. Address Alias: s
     */
    uint16_t wordNumber = 0;

    /**
     * @brief bit number. Address Alias: b
     */
    uint16_t bitNumber = 0;

    /**
     * @brief
     *
     * @param address
     * @return
     * @author Paul Grahek
     */
    static DataAddress parseDataAddressString(std::string address);

    /**
     * @brief
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    static std::string createDataAddressString(DataAddress addr);
};
