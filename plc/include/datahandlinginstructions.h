//
// Created by paulp on 2/26/2019.
//

#pragma once

#include <bitset>
#include <math.h>
#include "dataddress.h"

#define _USE_MATH_DEFINES
namespace Instructions {
    /**
     * @brief Convert to BCD (TOD) - Convert 16-bit integers into BCD values
     * @param val
     * @return
     */
    std::bitset<16> TOD(int16_t val, DataAddress dest);

    /**
     * @brief Convert From BCD (FRD) - Convert BCD values to 16-bit integers
     * @param bcd
     * @return
     */
    int16_t FRD(std::bitset<16> bcd, DataAddress dest);

    /**
     * @brief Radians To Degrees (DEG) - Convert from Radians to Degrees
     * @param source Either integer and/or floating point value
     * @param destination address of the word where data is to be stored
     * @return
     * @author Paul Grahek
     */
    double DEG(double rad, DataAddress dest);
    double DEG(int rad, DataAddress dest);

    /**
     * @brief Degrees to Radians (RAD) - Convert from Degrees to Radians
     * @param source Either integer and/or floating point value
     * @param destination address of the word where data is to be stored
     * @return
     * @author Paul Grahek
     */
    double RAD(double deg, DataAddress dest);
    double RAD(int deg, DataAddress dest);


}
