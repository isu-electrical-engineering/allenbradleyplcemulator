//
// Created by paulp on 2/26/2019.
//

#pragma once

#include <array>
#include <bitset>
#include "types.h"
#include "dataddress.h"
#include <unordered_map>

struct DataFile {
public:
    virtual bool setBit(DataAddress addr, bit val) = 0;
    virtual bit getBit(DataAddress addr) = 0;
    virtual bool validateAddress(DataAddress addr) = 0;
    virtual word getWord(DataAddress addr) = 0;
    virtual bool setWord(DataAddress addr, word val) = 0;
protected:
    DataType type;
};



