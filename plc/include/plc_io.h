//
// Created by paulgrahek on 8/21/19.
//
#pragma once

#include <thread>
#include <shared_mutex>
#include <future>
#include "io_buffer.h"
#include "webinterface.h"

class PLC_IO {
private:
    std::atomic_bool runThread;
    std::thread OutputThread;
    std::thread InputThread;
public:


    PLC_IO(bool autostart = true);

    ~PLC_IO();

    void Stop();

    void Start();
private:
    /**
     * @brief Loop for monitoring and controlling input connections
     * The input loop will serve as the "Client" in the Modbus relationship
     *
     * @author Paul Grahek
     */
    void inputLoop();

    /**
     * @brief Input Logic to be looped through by Input Loop
     * @author Paul Grahek
     */
    void inputLogic();

    /**
     * @brief Loop for monitoring and controlling output connections
     * The output loop will serve as the "Server" in the modbus relationship
     * @author Paul Grahek
     */
    void outputLoop();

    /**
     * @brief Output Logic to be looped through by Output Loop
     * @author Paul Grahek
     */
    void outputLogic();


    /**
     * @brief Indicates the interpreter has the ability to modify internal members of the PLC
     * @author Paul Grahek
     */
    friend class Interpreter;

    friend class Console;

    friend class WebInterface;
};