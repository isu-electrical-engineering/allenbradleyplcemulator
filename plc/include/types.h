//
// Created by paulgrahek on 2/25/19.
//

#pragma once

#include <string>
#include <stdint.h>
#include <modbus/modbus.h>
#include <map>

typedef bool bit;
typedef uint8_t byte;
typedef uint16_t word;

constexpr bit zero = 0;
constexpr bit one = 1;

enum DataType : int{ Output = 0, Input = 1, Status = 2, Bit = 3, Timer = 4, Counter = 5, Control = 6, Integer = 7, FloatingPoint = 8, String = 9, ASCII = 10};

const char DataTypeChars[] { 'O', 'I', 'S','B', 'T', 'C', 'R','N','F'};

enum AddressableBits : int {
    /** Counter Control Bits */
    CU = 15, CD = 14, /*DN = 13,*/ OV = 12, UN = 11, UA = 10,
    /** Control Data Bits*/
    /*EN = 15,*/ EU = 14, /*DN = 13,*/ EM = 12, ER = 11, UL = 10, IN = 9, FD = 8,
    /** Status Bits */
    Carry = 1, Overflow = 1, Zero = 2, Sign = 3,
};

/**
 * Used to translate enumerated values used in writing instructions to their indexed equivalents
 */
extern std::map<std::string,int> EnumerationIndexMap;

//enum InstructionEnums : int {
//    XIC, XIO, OTE
//};

//const std::string InstructionStrings[] {
//    "XIC","XIO","OTE"
//};

enum {
    TCP,
    RTU
};

constexpr int MAX_PROGRAM_FILES = 256;
/**
 * @brief Returns offset for Registers stored in 1D arrays given slot and word
 * @param slot
 * @param word
 * @return slot * slotCount + word
 * @author Paul Grahek
 */
int getRegisterOffset(int slot, int word);

constexpr bit GetBit(word wrd, int index){
    return ((wrd >> index) & 1);
}

constexpr word SetBit(word wrd, int index, bit val){
    return ((wrd & ~(1 << index)) | val << index);
}

/**
 * Fix for libmodbus macro issues on Fedora
 */
#ifndef MODBUS_MAX_WR_WRITE_REGISTERS
#define MODBUS_MAX_WR_WRITE_REGISTERS MODBUS_MAX_RW_WRITE_REGISTERS
#endif
#ifndef MODBUS_MAX_RW_WRITE_REGISTERS
#define MODBUS_MAX_RW_WRITE_REGISTERS MODBUS_MAX_WR_WRITE_REGISTERS
#endif

#define G_MSEC_PER_SEC 1000

