//
// Created by paulp on 4/22/2019.
//

#pragma once
#include "datafile.h"

constexpr uint16_t COUNTER_CU = 15;
constexpr uint16_t COUNTER_CD = 14;
constexpr uint16_t COUNTER_DN = 13;
constexpr uint16_t COUNTER_OV = 12;
constexpr uint16_t COUNTER_UN = 11;
constexpr uint16_t COUNTER_UA = 10;
constexpr uint16_t COUNTER_CON = 0;
constexpr uint16_t COUNTER_PRE = 1;
constexpr uint16_t COUNTER_ACC = 2;
constexpr uint16_t COUNTER_ElementCount = 256;
constexpr uint16_t COUNTER_WordCount = 3;
constexpr uint16_t COUNTER_BitCount = 16;
constexpr uint16_t COUNTER_MinElement = 0;
constexpr uint16_t COUNTER_MinWord = 0;
constexpr uint16_t COUNTER_MinBit = 0;

struct CounterFile : DataFile{
    std::array<std::array<std::bitset<COUNTER_BitCount>,3>,256> counterData;
public:

    /**
     * @brief Default Constructor
     *
     * Sets type to Output
     * Sets Filenumber to Output (0)
     *
     * @author Paul Grahek
     */
    CounterFile() {}

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setBit(DataAddress addr, bit val) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bit getBit(DataAddress addr) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bool validateAddress(DataAddress addr) override;

    /**
     * @brief
     * @param addr
     * @return
     * @author Paul Grahek
     */
    word getWord(DataAddress addr) override;

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setWord(DataAddress addr, word val) override;

};
