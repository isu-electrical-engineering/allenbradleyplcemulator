//
// Created by paulp on 4/22/2019.
//

#include "datafile.h"

constexpr int TIMER_EN = 15;
constexpr int TIMER_TT = 14;
constexpr int TIMER_DN = 13;
constexpr int TIMER_PRE = 1;
constexpr int TIMER_ACC = 2;
constexpr int TIMER_ElementCount = 256;
constexpr int TIMER_WordCount = 3;
constexpr int TIMER_BitCount = 16;
constexpr int TIMER_MinElement = 0;
constexpr int TIMER_MinWord = 0;
constexpr int TIMER_MinBit = 0;

struct TimerFile : DataFile{
    std::array<std::array<std::bitset<TIMER_BitCount>,3>,256> timerData;
public:

    /**
     * @brief Default Constructor
     *
     * Sets type to Output
     * Sets Filenumber to Output (0)
     *
     * @author Paul Grahek
     */
    TimerFile() {}

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setBit(DataAddress addr, bit val) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bit getBit(DataAddress addr) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bool validateAddress(DataAddress addr) override;

    /**
     * @brief
     * @param addr
     * @return
     * @author Paul Grahek
     */
    word getWord(DataAddress addr) override;

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setWord(DataAddress addr, word val) override;
};