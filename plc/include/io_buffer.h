//
// Created by paulgrahek on 4/10/19.
//

#pragma once

#include "io_file.h"
#include <atomic>
/**
 * @brief Primary IO manager between PLC and outside world
 * @author Paul Grahek
 */
class IO_Buffer {
private:
//    std::array<std::atomic<std::bitset<IO_TerminalCount>>,AB_MAX_READ_REGISTERS> terminals;
    std::array<std::bitset<IO_TerminalCount>,AB_MAX_READ_REGISTERS> terminals;
    std::array<std::bitset<IO_TerminalCount>,AB_MAX_READ_REGISTERS> forcedTerminals;
    std::array<std::bitset<IO_TerminalCount>,AB_MAX_READ_REGISTERS> forcedTerminalValues;

public:
    /**
     * @brief returns current register value. Value returned is determined by whether an individual bit is
     * forced or not
     * @param slot
     * @param word
     * @return
     * @author Paul Grahek
     */
    uint16_t getRegister(int slot, int word);

    /**
     * @brief returns current ternminal value. Value returned is determined by whether an individual bit is
     * forced or not
     * @param slot
     * @param word
     * @param terminal
     * @return
     * @author Paul Grahek
     */
    bit getTerminal(int slot, int word, int terminal);

    /**
     * @brief returns terminals that are currently forced
     * @param slot
     * @param word
     * @return
     * @author Paul Grahek
     */
    uint16_t getForcedRegister(int slot, int word);

    /**
     * @brief returns whether a terminal is forced
     * @param slot
     * @param word
     * @param terminal
     * @return
     * @author Paul Grahek
     */
    bit getForcedTerminal(int slot, int word, int terminal);

    /**
     * @brief sets the value of a register
     * @param slot
     * @param word
     * @param reg
     * @author Paul Grahek
     */
    void setRegister(int slot, int word, uint16_t reg);

    /**
     * @brief sets the value of a terminal
     * @param slot
     * @param word
     * @param terminal
     * @param val
     * @author Paul Grahek
     */
    void setTerminal(int slot, int word, int terminal, bit val);

    /**
     * @brief sets a forced value of a register
     * @param slot
     * @param word
     * @param reg
     * @author Paul Grahek
     */
    void setForcedRegister(int slot, int word, uint16_t reg);

    /**
     * @brief sets a forced value on a terminal
     * @param slot
     * @param word
     * @param terminal
     * @param val
     * @author Paul Grahek
     */
    void setForcedTerminal(int slot, int word, int terminal, bit val);

    /**
     * @brief unsets a forced register, setting return of getRegister to actual value
     * @param slot
     * @param word
     * @author Paul Grahek
     */
    void unsetForcedRegister(int slot, int word);

    /**
     * @brief unset a forced terminal, setting return of getTerminal to actual value
     * @param slot
     * @param word
     * @param terminal
     * @author Paul Grahek
     */
    void unsetForcedTerminal(int slot, int word, int terminal);
};
