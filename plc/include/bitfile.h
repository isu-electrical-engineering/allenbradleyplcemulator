//
// Created by paulgrahek on 3/15/19.
//

#pragma once

#include "datafile.h"

constexpr int BIT_BitCount = 16;
constexpr int BIT_MinBit = 0;
constexpr int BIT_WordCount = 256;
constexpr int BIT_MinWord = 0;

struct BitFile : DataFile{
    std::array<std::bitset<16>,256> bits;
public:

    /**
     * @brief Default Constructor
     *
     * Sets type to Output
     * Sets Filenumber to Output (0)
     *
     * @author Paul Grahek
     */
    BitFile() {}

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setBit(DataAddress addr, bit val) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bit getBit(DataAddress addr) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bool validateAddress(DataAddress addr) override;

    /**
     *
     * @param addr
     */
    void normalizeAddress(DataAddress &addr);

    /**
     * @brief
     * @param addr
     * @return
     * @author Paul Grahek
     */
    word getWord(DataAddress addr) override;

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setWord(DataAddress addr, word val) override;
};
