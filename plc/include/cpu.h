//
// Created by paulgrahek on 8/21/19.
//
#pragma once
#include <thread>
#include <vector>
#include <atomic>
#include "types.h"
#include "dataddress.h"
#include "datafile.h"
#include "programfile.h"

class ProgramFile;

/**
 *
 */
class CPU {
private:
    std::atomic_bool runThread;
    int fileNumber = 0, rungNumber = 0, nestLevel = 0,
            branchLevel = 0, instructionNum = 0, stackInd = 0;
    std::array<bit,256> stack;
    std::vector<int> previousBranchLevels{};
    std::array<std::shared_ptr<DataFile>,256> dataFiles;
    std::array<std::shared_ptr<ProgramFile>, 256> programFiles;
    std::thread CPUThread;
    std::atomic_int faultCode{0};

    /**
     * @brief Set the Fault Code to the given value
     * @param faultCode
     */
    void set_fault_code(int faultCode);

    /**
     * @brief Startup function. Initializes components releated to the execution of the PLC.
     * @author Paul Grahek
     */
    void startup();

    /**
     * @brief primary execution loop for PLC. Emulates RW & Ladder Logic functions
     * @author Paul Grahek
     */
    void loop();

    /**
     * @brief Executes instructions related to the ladder logic of the PLC.
     * @author Paul Grahek
     */
    void ladderLogic();

    /**
     * @brief Reads in Input Terminals and sets data in the Input file accordingly
     * @author Paul Grahek
     */
    void read();

    /**
     * @brief Writes current Output file state to Output Terminals
     * @author Paul Grahek
     */
    void write();

    /**
     * @brief Resets the Processor Files and Reloads from disk
     * @author Paul Grahek
     */
    void resetProcessorFiles();

    void run();
    
    /**
     * @brief Indicates the interpreter has the ability to modify internal members of the CPU
     * @author Paul Grahek
     */
    friend class Interpreter;

public:
    void Stop();

    void Start();

    CPU(bool autostart = true);

    ~CPU();
    /**
     * @brief Gets a data bit given an address
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bit getDataBit(DataAddress addr);

    /**
     * @brief Sets a data bit given an address
     * @param addr
     * @param val
     * @return
     * @author Paul Grahek
     */
    bool setDataBit(DataAddress addr, bit val);

    /**
     * @brief Gets a data word given an address
     * @param addr
     * @return
     * @author Paul Grahek
     */
    word getDataWord(DataAddress addr);

    /**
     * @brief Sets a data word given an address
     * @param addr
     * @param val
     * @return
     * @author Paul Grahek
     */
    bool setDataWord(DataAddress addr, word val);

    void setStackInd(int stackInd);

    int getFileNumber() const;

    void setFileNumber(int fileNumber);

    void setRungNumber(int rungNumber);

    void setNestLevel(int nestLevel);

    void setBranchLevel(int branchLevel);

    void setInstructionNum(int instructionNum);

    int getRungNumber() const;

    int getNestLevel() const;

    int getBranchLevel() const;

    int getInstructionNum() const;

    int getStackInd() const;

    const std::array<bit, 256> &getStack() const;

    void setStack(const std::array<bit, 256> &stack);

    void setStackValue(int stackInd, bit value);

    bit getStackValue(int stackInd) const;

    /// Control Instructions (Special instructions for operating the CPU emulator)
    void BR();
    void BN();
    void BE();
    void RNG();

    /// Instructions below are based off
    /// the SLC 500 Instruction Set Reference Manual
    // Basic Instructions
    void XIC(DataAddress addr);
    void XIO(DataAddress addr);
    void OTE(DataAddress addr);
    void OTL(DataAddress addr);
    void OTU(DataAddress addr);
    void OSR(DataAddress addr);

    // Counter & Timer Instructions
    void TON(DataAddress addr);
    void TOF(DataAddress addr);
    void RTO(DataAddress addr);
    void CTU(DataAddress addr);
    void CTD(DataAddress addr);
    void HSC(DataAddress addr);
    void RES(DataAddress addr);

    // Comparison Instructions
    void EQU(DataAddress a, DataAddress b);
    void EQU(DataAddress a, int b);
    void NEQ(DataAddress a, DataAddress b);
    void NEQ(DataAddress a, int b);
    void LES(DataAddress a, DataAddress b);
    void LES(DataAddress a, int b);
    void LEQ(DataAddress a, DataAddress b);
    void LEQ(DataAddress a, int b);
    void GRT(DataAddress a, DataAddress b);
    void GRT(DataAddress a, int b);
    void GEQ(DataAddress a, DataAddress b);
    void GEQ(DataAddress a, int b);
};
