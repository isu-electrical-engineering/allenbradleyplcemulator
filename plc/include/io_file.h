//
// Created by paulgrahek on 3/15/19.
//

#pragma once

#include "dataddress.h"
#include "datafile.h"

constexpr int IO_MinSlot = 1; // 1
constexpr int IO_SlotCount = 31; // 31
constexpr int IO_MinWord = 0;
constexpr int IO_WordCount = 256;
constexpr int IO_MinTerminal = 0;
constexpr int IO_TerminalCount = 16;
constexpr int AB_MAX_READ_REGISTERS = IO_SlotCount * IO_WordCount;

/**
 * @brief IOFile- DataFile that manages either input or output
 * @author Paul Grahek
 */
struct IOFile : DataFile{

private:
    std::array<std::array<std::bitset<
        IO_TerminalCount>,IO_WordCount>,IO_SlotCount> terminals;
    std::array<std::array<std::bitset<
            IO_TerminalCount>,IO_WordCount>,IO_SlotCount> validTerminals;
    friend class Printer;

public:


    /**
     * @brief Default Constructor
     *
     * Sets type to Output
     * Sets Filenumber to Output (0)
     *
     * @author Paul Grahek
     */
    IOFile(bool isInput);

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setBit(DataAddress addr, bit val) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bit getBit(DataAddress addr) override;

    /**
     *
     * @param addr
     * @return
     * @author Paul Grahek
     */
    bool validateAddress(DataAddress addr) override;

    /**
     * @brief
     * @param addr
     * @return
     * @author Paul Grahek
     */
    word getWord(DataAddress addr) override;

    /**
     *
     * @param addr
     * @param val
     * @author Paul Grahek
     */
    bool setWord(DataAddress addr, word val) override;
};
