//
// Created by paulgrahek on 3/8/19.
//

#pragma once

#include <sstream>
#include <iterator>
#include <fstream>
#include <sys/stat.h>
#include "instruction.h"

/**
 * @brief The Interpreter parses instructions from program files for the PLC to run
 * @author Paul Grahek
 */
class Interpreter {
    static std::chrono::high_resolution_clock::time_point refreshTime;
public:
    /**
     * @brief Parses a line and returns the corresponding instruction
     * @param line
     * @return
     * @author Paul Grahek
     */
    static std::shared_ptr<Instruction> parseLine(const std::string& line);

    /**
     * @brief reads a program file
     * @param fileDirectory
     * @param fileName
     * @author Paul Grahek
     */
    static void readProgramFiles(CPU& cpu, std::string programFilesDirectory);

    /**
     * @brief Reads a program file in given a full file path
     * @param fullFileName
     * @return
     * @author Paul Grahek
     */
    static std::shared_ptr<ProgramFile> readProgramFile(std::string fullFileName);

    /**
     * @brief refreshes program files
     * @author Paul Grahek
     */
    static void refreshProgramFiles(CPU& cpu);

    /**
     * @brief refreshes data files
     * @author Paul Grahek
     */
    static void refreshDataFiles(CPU& cpu);
};
