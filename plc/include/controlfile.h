//
// Created by paulp on 9/9/2019.
//

#pragma once
#include "datafile.h"

class ControlFile : public DataFile {
    bool setBit(DataAddress addr, bit val) override;

    bit getBit(DataAddress addr) override;

    bool validateAddress(DataAddress addr) override;

    word getWord(DataAddress addr) override;

    bool setWord(DataAddress addr, word val) override;

};

