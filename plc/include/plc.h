//
// Created by paulgrahek on 2/25/19.
//

#pragma once

#include <iostream>
#include <thread>
#include <deque>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include "dataddress.h"
#include "datafile.h"
#include "programfile.h"
#include "cpu.h"
#include "plc_io.h"


/**
 * @brief encapsulates all of the data associated with the execution of a PLC
 * @author Paul Grahek
 */
class PLC{
private:
    static std::unique_ptr<CPU> cpu;
    static std::unique_ptr<PLC_IO> cpuIo;
    static std::unique_ptr<WebInterface> webInterface;
    static std::atomic<bool> runThread;
    static IO_Buffer inputBuffer;
    static std::shared_timed_mutex outputMutex;
    static IO_Buffer outputBuffer;
    static std::shared_timed_mutex inputMutex;
public:

    static CPU& getCPU();

    static PLC_IO& getCPU_IO();

    /**
     *
     */
    static void SetUp(bool autostart);

    static void Start();

    static void Stop();

    static std::shared_timed_mutex &getInputMutex();

    static IO_Buffer &getInputBuffer();

    static std::shared_timed_mutex &getOutputMutex();

    static IO_Buffer &getOutputBuffer();

    /**
     *
     */
    static void TearDown();

    /**
     * @brief Resets the PLC Loops and Pulls the configuration again
     * @author Paul Grahek
     */
    static void Reset();

    static bool isRunning();





};
