//
// Created by paulgrahek on 3/29/19.
//

#pragma once

#include <thread>
#include <mutex>
#include <atomic>


/**
 * @brief ws_connection - contains data about a websocket connection
 */
struct ws_connection {
    int status;
    int socket;
    int port;
};

/**
 * @brief Web Interface - Uses sockets to communicate with web interfaces to provide data about the PLC
 * @author Paul Grahek
 */
class WebInterface{
    std::atomic_bool runThread;
    std::thread webthread;
    ws_connection currConnection;
    /**
     * @brief Loop for webinterface to service requests on
     * @author Paul Grahek
     */
    void loop();

    /**
     * @brief Builds a connection to a web socket and returns a struct with related information
     * @author Paul Grahek
     */
    struct ws_connection ws_connect();
public:
    WebInterface(bool autostart = true);

    ~WebInterface();

    void Start();


    void Stop();
};