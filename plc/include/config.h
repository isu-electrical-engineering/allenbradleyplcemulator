//
// Created by paulp on 4/1/2019.
//

#pragma once

#include <string>
#include <mutex>
#include <shared_mutex>

#define json_hostname "hostname"
#define json_inputaddress "inputaddress"
#define json_inputport "inputport"
#define json_outputaddress "outputaddress"
#define json_outputport "outputport"
#define json_webaddress "webaddress"
#define json_webport "webport"

/**
 * @brief Contains data about the current Emulator's configuration
 * @author Paul Grahek
*/
class Configuration {
private:
    static Configuration* instance;
    static std::mutex instanceMutex;
    std::shared_timed_mutex configMutex;
    Configuration();
    std::string Hostname = "virtual0";
    std::string InputAddress = "127.0.0.1";
    int InputPort = 11502;
    std::string OutputAddress = "127.0.0.1";
    int OutputPort = 11502;
    std::string WebAddress = "127.0.0.1";
    int WebPort = 10000;
    std::string ConfigFilePath = "./configfiles/default.json";

public:
    /**
     * @brief Gets the current configuration instance
     * @return
     * @author Paul Grahek
     */
    static Configuration& Instance();

    /**
     * @brief Reloads configuration from disk
     * @author Paul Grahek
     */
    void ReloadConfig(const char* newFilePath);

    /**
     * @brief gets the current hostname
     * @return
     * @author Paul Grahek
     */
    std::string getHostname();

    /**
     * @brief gets the current input address
     * @return
     * @author Paul Grahek
     */
    std::string getInputAddress();

    /**
     * @brief gets the current input port
     * @return
     * @author Paul Grahek
     */
    int getInputPort();

    /**
     * @brief gets the current output address
     * @return
     * @author Paul Grahek
     */
    std::string getOutputAddress();

    /**
     * @brief gets the current output port
     * @return
     * @author Paul Grahek
     */
    int getOutputPort();

    /**
     * @brief gets the current web address
     * @return
     * @author Paul Grahek
     */
    std::string getWebAddress();

    /**
     * @brief gets the current web port
     * @return
     * @author Paul Grahek
     */
    int getWebPort();
};

