//
// Created by paulgrahek on 3/1/19.
//

#pragma once


#include <vector>
#include <array>
#include "instruction.h"
#include <memory>
#include <chrono>

class Instruction;
/**
 * @brief Contains data regarding instructions loaded into a particular Program File
 */
struct ProgramFile {
    std::vector<std::shared_ptr<Instruction>> instructions;
//    std::chrono::system_clock::time_point lastModified = std::chrono::system_clock::time_point::min();
    std::string fullFileName = "";
};
////void fillFunctionArray();
