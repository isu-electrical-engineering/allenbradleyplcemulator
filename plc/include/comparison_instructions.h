//
// Created by paulp on 2/26/2019.
//

#pragma once

#include "dataddress.h"
#include "plc.h"

namespace Instructions {
//    /**
//    * @brief Equal (EQU)- Test Whether two values are equal.
//    * @param A Address for comparison
//    * @param B Address for comparison
//    * @return whether A & B's values are equal
//    * @author Paul Grahek
//    */
//    bool EQU(DataAddress A, DataAddress B);
//
//    /**
//     * @brief Equal (EQU)- Test Whether two values are equal.
//     * @param A Address for comparison
//     * @param Bval Value for comparison
//     * @return whether A & B's values are equal
//     * @author Paul Grahek
//     */
//    bool EQU(DataAddress A, word Bval);
//
//    /**
//     * @brief Not Equal(NEQ)- Test Whether two values are not equal.
//     * @param A Address for comparison
//     * @param B Address for comparison
//     * @return whether A & B's values are not qual
//     * @author Paul Grahek
//     */
//    bool NEQ(DataAddress A, DataAddress B);
//
//    /**
//     * @brief Not Equal(NEQ)- Test Whether two values are not equal.
//     * @param A Address for comparison
//     * @param Bval Value for comparison
//     * @return whether A & B's values are not equal
//     * @author Paul Grahek
//     */
//    bool NEQ(DataAddress A, word Bval);
//
};
