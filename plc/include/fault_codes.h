//
// Created by paulp on 9/13/2019.
//

#pragma once
enum ProcessorFaultCodes : int {
    INSUFFICIENT_REVISION_LEVEL = 0x0010,
    NO_PROGRAM_FILE_NUMBER_2 = 0x0011,
    INCOMPATIBLE_USER_PROGRAM = 0x0018,
    MISSING_OR_DUPLICATE_LABEL = 0x0019,

};
