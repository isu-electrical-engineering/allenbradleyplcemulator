//
// Created by paulgrahek on 3/11/19.
//

#pragma once


#include "cpu.h"
#include <functional>
#include <utility>
class CPU;
/**
 * @brief Base class for Instructions. Allows for generic call during Program File loop
 * @author Paul Grahek
 */
class Instruction {
    std::function<void(CPU&)> funct;
public:
    static std::map<std::string,std::function<void(CPU&)>> VoidInstructionStringFunctionMap;
    static std::map<std::string,std::function<void(CPU&,DataAddress)>> SingleAddressInstructionStringFunctionMap;

    Instruction() = default;

    explicit Instruction(std::function<void(CPU&)> funct){
        this->funct = funct;
    }
    /**
     * @brief Executes the function of the Instruction
     * @author Paul Grahek
     */
    static void init();

    /**
     * @brief Initalizes data regarding All Instructions
     * @author Paul Grahek
     */
    virtual void run(CPU& cpu);








};