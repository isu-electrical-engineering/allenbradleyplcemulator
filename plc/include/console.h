//
// Created by paulp on 3/11/2019.
//

#pragma once


#include <string>
#include <iostream>
#include "dataddress.h"

class Console {
public:
    /**
     * @brief Primary Console Loop For Input
     * @author Paul Grahek
     */
    static void Loop();
};

