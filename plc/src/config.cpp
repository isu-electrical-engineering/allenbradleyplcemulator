//
// Created by paulp on 4/1/2019.
//

#include "config.h"
#include "nlohmann/json.hpp"
#include <fstream>

using json = nlohmann::json;

Configuration* Configuration::instance = 0;
std::mutex Configuration::instanceMutex;

Configuration &Configuration::Instance() {
    if(!instance){
        std::lock_guard<std::mutex> lock(instanceMutex);
        if(!instance){
            instance = new Configuration();
        }
    }
    return *instance;
}

void Configuration::ReloadConfig(const char* newFilePath) {
    //Grab config file and parse to json
    std::ifstream configFile(newFilePath);
    // If the path is good, change the config path.
    // Otherwise, the previous config path will be loaded
    if(configFile.is_open() && !configFile.bad()){
        ConfigFilePath = std::string(newFilePath);
        configFile.close();
    }
    // Load the current config file path
    configFile = std::ifstream(ConfigFilePath);
    if(!configFile.is_open() || configFile.bad()) return;
    json configJson;
    configFile >> configJson;

    std::string strBuffer = "";
    int intBuffer = 0;
    configJson.at(json_inputaddress).get_to(strBuffer);
    if(!strBuffer.empty()){
        InputAddress = strBuffer;
    }
    configJson.at(json_inputport).get_to(intBuffer);
    if(intBuffer > 0){
        InputPort = intBuffer;
    }

    configJson.at(json_outputaddress).get_to(strBuffer);
    if(!strBuffer.empty()){
        OutputAddress = strBuffer;
    }
    configJson.at(json_outputport).get_to(intBuffer);
    if(intBuffer > 0){
        OutputPort = intBuffer;
    }

    configJson.at(json_webaddress).get_to(strBuffer);
    if(!strBuffer.empty()){
        WebAddress = strBuffer;
    }
    configJson.at(json_webport).get_to(intBuffer);
    if(intBuffer > 0){
        WebPort = intBuffer;
    }
}

Configuration::Configuration() {
    Hostname = "virtual0";
    InputAddress = "127.0.0.1";
    InputPort = 11502;
    OutputAddress = "127.0.0.1";
    OutputPort = 11502;
    WebAddress = "0.0.0.0";
    WebPort = 10000;
    ReloadConfig(ConfigFilePath.c_str());
}

std::string Configuration::getHostname() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return Hostname;
}

std::string Configuration::getInputAddress() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return InputAddress;
}

int Configuration::getInputPort() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return InputPort;
}

std::string Configuration::getOutputAddress() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return OutputAddress;
}

int Configuration::getOutputPort() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return OutputPort;
}

std::string Configuration::getWebAddress() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return WebAddress;
}

int Configuration::getWebPort() {
    std::shared_lock<std::shared_timed_mutex> lock(configMutex);
    return WebPort;
}


