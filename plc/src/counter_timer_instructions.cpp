//
// Created by paulgrahek on 9/9/19.
//
#include <timerfile.h>
#include <counterfile.h>
#include "cpu.h"

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::TON(DataAddress addr) {
    // If rung conditions are true ...
    if(stack[stackInd]){
        // Set DN bit if ACC is >= preset value
        addr.wordNumber = TIMER_PRE;
//        int presetVal = getDataWord()
        // Set Enable bit
        addr.wordNumber = 0;
        addr.bitNumber = TIMER_EN;
        setDataBit(addr,1);

    }
        // If rung conditions are false ...
    else{
        // Reset ACC value
        addr.wordNumber = TIMER_ACC;
        setDataWord(addr,0);
        // Reset DN, TT, EN
        addr.wordNumber = 0;
        addr.bitNumber = TIMER_DN;
        setDataBit(addr,0);
        addr.bitNumber = TIMER_TT;
        setDataBit(addr,0);
        addr.bitNumber = TIMER_EN;
        setDataBit(addr,0);
    }
}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::TOF(DataAddress addr) {

}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::RTO(DataAddress addr) {

}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::CTU(DataAddress addr) {
    bit stackValue = getStackValue(this->getStackInd());

    addr.wordNumber = COUNTER_CON;
    word control = getDataWord(addr);
    addr.wordNumber = COUNTER_PRE;
    int presetValue = getDataWord(addr);
    addr.wordNumber = COUNTER_ACC;
    int accumulatorValue = getDataWord(addr);
    bit CU = GetBit(control,COUNTER_CU), 
        DN = GetBit(control,COUNTER_DN),
        OV = GetBit(control,COUNTER_OV), 
        UN = GetBit(control,COUNTER_UN);
    // Add to the accumulator if the rung changes from false to true
    bit incrementACC = (stackValue > CU);
    accumulatorValue += incrementACC;
    if(incrementACC && accumulatorValue == -32768){
        OV = 1;
        UN = 0;
    }
    CU = stackValue;
    // Set Done Bit if ACC >= PRE
    DN = accumulatorValue >= presetValue;
    // Set updated control bits
    control = SetBit(control,COUNTER_DN,DN);
    control = SetBit(control,COUNTER_CU,CU);
    control = SetBit(control,COUNTER_OV,OV);
    control = SetBit(control,COUNTER_UN,UN);
    addr.wordNumber = COUNTER_CON;
    setDataWord(addr,control);
    addr.wordNumber = COUNTER_ACC;
    setDataWord(addr,accumulatorValue);
}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::CTD(DataAddress addr) {

}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::HSC(DataAddress addr) {

}

/**
 * @brief
 * @param addr
 * @author Paul Grahek
 */
void CPU::RES(DataAddress addr) {
    switch(addr.type){
        case Timer:
            addr.wordNumber = TIMER_ACC;
            setDataWord(addr,0);
            addr.wordNumber = 0;
            addr.bitNumber = TIMER_DN;
            setDataBit(addr,0);
            addr.bitNumber = TIMER_TT;
            setDataBit(addr,0);
            addr.bitNumber = TIMER_EN;
            setDataBit(addr,0);
            break;
        case Counter:
            addr.wordNumber = COUNTER_ACC;
            setDataWord(addr,0);

            addr.wordNumber = 0;
            addr.bitNumber = COUNTER_OV;
            setDataBit(addr,0);
            addr.bitNumber = COUNTER_UN;
            setDataBit(addr,0);
            addr.bitNumber = COUNTER_DN;
            setDataBit(addr,0);
            addr.bitNumber = COUNTER_CU;
            setDataBit(addr,0);
            addr.bitNumber = COUNTER_CD;
            setDataBit(addr,0);
            break;
        case Control:
//            addr.wordNumber = CONTROL_POS;
//            setDataWord(addr,0);
//
//            addr.wordNumber = 0;
//            addr.bitNumber = CONTROL_EN;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_EU;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_DN;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_EM;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_ER;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_UL;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_IN;
//            setDataBit(addr,0);
//            addr.bitNumber = CONTROL_FD;
//            setDataBit(addr,0);
            break;
    }
}