//
// Created by paulgrahek on 3/8/19.
//

#include "types.h"
#include "timerfile.h"
#include "io_file.h"
std::map<std::string,int> EnumerationIndexMap = {
    /** Timer Control Bits */
    {"EN",TIMER_EN}, {"TT",TIMER_TT}, {"DN",TIMER_DN},
    /** Timer Control Words */
    {"PRE",TIMER_PRE},{"ACC",TIMER_ACC},
};

int getRegisterOffset(int slot, int word) {
    return slot * IO_SlotCount + word;
}

