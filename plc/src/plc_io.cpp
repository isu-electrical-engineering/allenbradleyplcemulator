//
// Created by paulp on 4/1/2019.
//

#include <sys/time.h>
#include "plc_io.h"
#include "config.h"
#include "io_file.h"
#include "coutsync.h"
#include <unistd.h>
#include <shared_mutex>
#include <plc.h>
#include <sys/socket.h>
#include <future>
#include <boost/log/trivial.hpp>
#include <fcntl.h>
void PLC_IO::outputLoop() {
    while(runThread){
        // IO Functions
        try{
            outputLogic();
        }
        catch (int e){}
    }
    BOOST_LOG_TRIVIAL(info) << "Exiting CPU Output Thread";
}


uint32_t gettime_ms(void)
{
    struct timeval tv;
    gettimeofday (&tv, NULL);

    return (uint32_t) tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

void PLC_IO::inputLoop() {
    sleep(1);
    //Primary running loop
    int sleepDelay = 100;
    std::chrono::milliseconds sleepInterval(sleepDelay);
    while(runThread){
        auto nextStop = std::chrono::system_clock::now() + sleepInterval;
        // IO Functions
        inputLogic();
        // Sleep until next interval (prevents excessive CPU time)
        std::this_thread::sleep_until(nextStop);
    }
    BOOST_LOG_TRIVIAL(info) << "Exiting CPU Input Thread";
}

void PLC_IO::inputLogic() {
    std::string CurrentInputAddress = Configuration::Instance().getInputAddress();
    int CurrentInputPort = Configuration::Instance().getInputPort();
    BOOST_LOG_TRIVIAL(info) << "CPU Input Address:" << CurrentInputAddress;
    BOOST_LOG_TRIVIAL(info) << "CPU Input Port:" << CurrentInputPort;
//    modbus_t *ctx;
    int rc;
//    int use_backend = TCP;
    modbus_t* inputCtx = modbus_new_tcp(CurrentInputAddress.c_str(), CurrentInputPort);
//    modbus_set_debug(ctx,true);
    if (modbus_connect(inputCtx) == -1) {
        BOOST_LOG_TRIVIAL(error) << "Connection failed:" << modbus_strerror(errno);
        modbus_free(inputCtx);
        return;
    }

    /* Allocate and initialize the memory to store the registers */
    const int max_read_count = 64; // Should not exceed 121 or libmodbus will not work (violates spec)
    uint16_t* receive_buffer = new uint16_t[max_read_count];
    uint16_t* input_registers = new uint16_t[AB_MAX_READ_REGISTERS * sizeof(uint16_t)];

    bool breakLoop = false;
    int sleepDelay = 100;
    std::chrono::milliseconds sleepInterval(sleepDelay);
    while(!breakLoop && runThread){
        auto nextStop = std::chrono::system_clock::now() + sleepInterval;
        // Read in the input buffer and place it in the input loop's buffer
        for(int reg = 0; reg < AB_MAX_READ_REGISTERS && runThread; reg+=max_read_count) {
//            int read_count = (AB_MAX_READ_REGISTERS - reg) % max_read_count;
            int read_count = max_read_count;
            rc = modbus_read_registers(inputCtx, reg, read_count, receive_buffer);
            if (rc == -1) {
                BOOST_LOG_TRIVIAL(error) << "READ:" << modbus_strerror(errno);
                breakLoop = true;
                break;
            }
            for(int i = 0; i < read_count;i++){
                input_registers[reg+i] = receive_buffer[i];
            }
            if(!runThread) break;
        }
        // Lock the input buffer mutex and move the input loop's buffer to the
        // Input Buffer
        {
            std::unique_lock<std::shared_timed_mutex> lock(PLC::getInputMutex());
            DataAddress inputFileAddr;
            inputFileAddr.type = Input;
            inputFileAddr.fileNumber = Input;
            for(uint16_t slot = IO_MinSlot; slot < IO_SlotCount; slot++){
                for(uint16_t word = IO_MinWord; word < IO_WordCount; word++){
                    inputFileAddr.elementNumber = slot;
                    inputFileAddr.wordNumber = word;
                    PLC::getInputBuffer().setRegister(slot,word,input_registers[getRegisterOffset(slot, word)]);
                }
            }
        }
        auto sleepDuration = nextStop - std::chrono::system_clock::now();
//        std::this_thread::sleep_for(sleepInterval);

    }




    /* Free the memory */
    delete[] input_registers;
    delete[] receive_buffer;

    /* Close the connection */
    modbus_close(inputCtx);
    modbus_free(inputCtx);
}

void PLC_IO::outputLogic() {
    std::string CurrentOutputAddress = Configuration::Instance().getOutputAddress();
    int CurrentOutputPort = Configuration::Instance().getOutputPort();
    BOOST_LOG_TRIVIAL(info) << "CPU Output Address:" << CurrentOutputAddress;
    BOOST_LOG_TRIVIAL(info) << "CPU Output Port:" << CurrentOutputPort;

    modbus_mapping_t *mb_mapping;
    int rc;
//    int use_backend = TCP;

    modbus_t* outputCtx = modbus_new_tcp(CurrentOutputAddress.c_str(), CurrentOutputPort);
    // Set indicator timeout so server doesn't wait on input too long.
    struct timeval response_timeout;
    response_timeout.tv_sec = 1;
    response_timeout.tv_usec = 0;
    modbus_set_indication_timeout(outputCtx,response_timeout.tv_sec, response_timeout.tv_usec);

    // Get socket and set to nonblocking (allows return)
    int outputSocket = modbus_tcp_listen(outputCtx, 1);
    int flags = fcntl(outputSocket, F_GETFL);
    fcntl(outputSocket, F_SETFL, flags | O_ASYNC);
    modbus_tcp_accept(outputCtx, &outputSocket);

    mb_mapping = modbus_mapping_new(AB_MAX_READ_REGISTERS * 16, 0,
                                    AB_MAX_READ_REGISTERS, 0);

    if (mb_mapping == NULL) {
        BOOST_LOG_TRIVIAL(error) <<"Failed to allocate the mapping:" << modbus_strerror(errno);
        modbus_free(outputCtx);
        return;
    }

    while(runThread) {
        uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];

        rc = modbus_receive(outputCtx, query);
        if (rc >= 0) {
            //Lock the mutex and read the current state of the output buffer
            //Into the modbus buffer
            {
                std::shared_lock<std::shared_timed_mutex> lock(PLC::getOutputMutex());
                DataAddress outputFileAddr;
                outputFileAddr.type = Output;
                outputFileAddr.fileNumber = Output;

                for(uint16_t slot = IO_MinSlot; slot < IO_SlotCount; slot++){
                    for(uint16_t word = IO_MinWord; word < IO_WordCount; word++){
                        outputFileAddr.elementNumber = slot;
                        outputFileAddr.wordNumber = word;
                        mb_mapping->tab_registers[slot*IO_SlotCount + word] = PLC::getOutputBuffer().getRegister(slot,word);
                    }
                }
            }
            /* Execute request */
            modbus_reply(outputCtx, query, rc, mb_mapping);
        } else {
            close(outputSocket);
            if(!runThread) break;
            outputSocket = modbus_tcp_listen(outputCtx, 1);
            fcntl(outputSocket, F_SETFL, flags | O_ASYNC);
            modbus_tcp_accept(outputCtx,&outputSocket);
        }
    }

    modbus_mapping_free(mb_mapping);
    close(outputSocket);
    modbus_free(outputCtx);
}



PLC_IO::~PLC_IO() {
    Stop();
}

void PLC_IO::Stop() {
    runThread = false;
    if(InputThread.joinable()){
        // Join the input thread first in case the PLC is looped back
        InputThread.join();
    }
    if(OutputThread.joinable()){
        // Stimulate the output socket so it loops and shuts down
        std::string OutputAddress = Configuration::Instance().getOutputAddress();
        int OutputPort = Configuration::Instance().getOutputPort();
        modbus_t* outputTimeoutCtx = modbus_new_tcp(OutputAddress.c_str(), OutputPort);
        modbus_connect(outputTimeoutCtx);
        modbus_close(outputTimeoutCtx);
        modbus_free(outputTimeoutCtx);
        OutputThread.join();
    }

}

PLC_IO::PLC_IO(bool autostart) {
    runThread = autostart;
    if(autostart){
        Start();
    }
}

void PLC_IO::Start() {
    runThread = true;
    if(!OutputThread.joinable()){
        OutputThread = std::thread(&PLC_IO::outputLoop, this);
    }
    if(!InputThread.joinable()){
        InputThread = std::thread(&PLC_IO::inputLoop, this);
    }
}
