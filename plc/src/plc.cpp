//
// Created by paulgrahek on 2/25/19.
//

#include "plc.h"
#include "interpreter.h"
#include "io_file.h"
#include "bitfile.h"
#include "config.h"
#include "coutsync.h"
#include <modbus/modbus.h>
#include <unistd.h>
#include <cstring>
#include <sys/time.h>
#include <shared_mutex>
#include <boost/log/trivial.hpp>

std::unique_ptr<CPU> PLC::cpu = 0;
std::unique_ptr<PLC_IO> PLC::cpuIo = 0;
std::unique_ptr<WebInterface> PLC::webInterface = 0;
std::atomic<bool> PLC::runThread;
IO_Buffer PLC::inputBuffer;
std::shared_timed_mutex PLC::outputMutex;
IO_Buffer PLC::outputBuffer;
std::shared_timed_mutex PLC::inputMutex;

void PLC::Reset() {
    TearDown();
    SetUp(true);
}

bool PLC::isRunning() {
    return runThread.load();
}


void PLC::TearDown() {
    BOOST_LOG_TRIVIAL(info) << "Tearing down PLC";
    runThread = false;
    cpu.reset(NULL);
    cpuIo.reset(NULL);
    webInterface.reset(NULL);
}

CPU &PLC::getCPU() {
    return *cpu;
}

PLC_IO &PLC::getCPU_IO() {
    return *cpuIo;
}

void PLC::SetUp(bool autostart) {
    BOOST_LOG_TRIVIAL(info) << "Setting Up PLC";
    runThread = autostart;
    cpu = std::make_unique<CPU>(autostart);
    cpuIo = std::make_unique<PLC_IO>(autostart);
    webInterface = std::make_unique<WebInterface>(autostart);
}

void PLC::Start() {
    runThread = true;
    cpu->Start();
    cpuIo->Start();
    webInterface->Start();
}

void PLC::Stop() {
    runThread = false;
    cpu->Stop();
    cpuIo->Stop();
    webInterface->Stop();
}

std::shared_timed_mutex &PLC::getInputMutex() {
    return inputMutex;
}

IO_Buffer &PLC::getInputBuffer() {
    return inputBuffer;
}

std::shared_timed_mutex &PLC::getOutputMutex() {
    return outputMutex;
}

IO_Buffer &PLC::getOutputBuffer() {
    return outputBuffer;
}

