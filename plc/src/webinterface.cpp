//
// Created by paulgrahek on 3/29/19.
//

#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include "webinterface.h"
#include "plc.h"
#include "config.h"
#include "coutsync.h"
#include "nlohmann/json.hpp"
#include "io_file.h"
#include <unistd.h>
#include <boost/log/trivial.hpp>

#define CHUNK_SIZE 256

void WebInterface::loop() {
    int newsockfd;
    struct sockaddr_in client_addr;
    currConnection = ws_connect();
    char clientAddr[INET_ADDRSTRLEN];
    socklen_t clientLen;

    while(runThread){
        clientLen = sizeof(client_addr);
        newsockfd = accept(currConnection.socket,(struct sockaddr*)& client_addr, &clientLen);
        if(newsockfd < 0) {
            BOOST_LOG_TRIVIAL(error) << strerror(errno);
//            perror("ERROR: Could not accept connection");
            continue;
        }
        char chunk[CHUNK_SIZE];
        int size_recv, total_size=0;
        std::string message = "";
        // Read input from socket
        // Help from: https://www.binarytides.com/receive-full-data-with-recv-socket-function-in-c/
        // TODO: Make this more stable
        while((size_recv = recv(newsockfd,chunk,CHUNK_SIZE,0)) > 0) {
            message.append(chunk,size_recv);
            bzero(chunk,CHUNK_SIZE);
            break;
            if(size_recv < CHUNK_SIZE) break;
        }

//        BOOST_LOG_TRIVIAL(info) << message;
        nlohmann::json jsonRequest = nlohmann::json::parse(message);
        if(jsonRequest.find("Get") != jsonRequest.end()){
            nlohmann::json jsonResponse;
            {
                std::shared_lock<std::shared_timed_mutex> inputlock(PLC::getInputMutex());
                std::shared_lock<std::shared_timed_mutex> outputlock(PLC::getOutputMutex());
                for(int slot = 0; slot < IO_SlotCount; slot++){
                    for(int word = 0; word < IO_WordCount; word++){
                        jsonResponse["Input"][slot][word]["Data"] = PLC::getInputBuffer().getRegister(slot,word);
                        jsonResponse["Input"][slot][word]["Forced"] = PLC::getInputBuffer().getForcedRegister(slot,word);
                        jsonResponse["Output"][slot][word]["Data"] = PLC::getOutputBuffer().getRegister(slot,word);
                        jsonResponse["Output"][slot][word]["Forced"] = PLC::getOutputBuffer().getForcedRegister(slot,word);
                    }
                }
            }
            std::string jsonString = jsonResponse.dump();
            send(newsockfd,jsonString.c_str(),jsonString.size(),MSG_NOSIGNAL);
        }
        else if (jsonRequest.find("Set") != jsonRequest.end()){
//            syncCout << "Set!" << syncEndl;
            try{
                int slot, word, terminal, value;
                slot = std::stoi(std::string(jsonRequest["Set"]["Slot"]));
                word = std::stoi(std::string(jsonRequest["Set"]["Word"]));
                terminal = std::stoi(std::string(jsonRequest["Set"]["Terminal"]));
                if(jsonRequest["Set"]["Value"] == ""){
                    if(jsonRequest["Set"]["Type"] == "Output"){
                        BOOST_LOG_TRIVIAL(debug) << "Unlock Output";
                        std::unique_lock<std::shared_timed_mutex> outputlock(PLC::getOutputMutex());
                        PLC::getOutputBuffer().unsetForcedTerminal(slot, word, terminal);
                    }
                    else if(jsonRequest["Set"]["Type"] == "Input"){
                        BOOST_LOG_TRIVIAL(debug) << "Unlock Input";
                        std::unique_lock<std::shared_timed_mutex> inputlock(PLC::getInputMutex());
                        PLC::getInputBuffer().unsetForcedTerminal(slot, word, terminal);

                    }
                }
                else{
                    value = std::stoi(std::string(jsonRequest["Set"]["Value"]));
                    if(jsonRequest["Set"]["Type"] == "Output"){
                        BOOST_LOG_TRIVIAL(debug) << "Lock Output";
                        std::unique_lock<std::shared_timed_mutex> inputlock(PLC::getOutputMutex());
                        PLC::getOutputBuffer().setForcedTerminal(slot, word, terminal, value);
                    }
                    else if(jsonRequest["Set"]["Type"] == "Input"){
                        BOOST_LOG_TRIVIAL(debug) << "Lock Input";
                        std::unique_lock<std::shared_timed_mutex> outputlock(PLC::getInputMutex());
                        PLC::getInputBuffer().setForcedTerminal(slot, word, terminal, value);
                    }
                }

            }
            catch (int e){
                BOOST_LOG_TRIVIAL(debug) << "Bad Conversion: " << e;
            }

        }

        shutdown(newsockfd, SHUT_RDWR);
        close(newsockfd);
    }
}

struct ws_connection WebInterface::ws_connect() {
    //Instantiate the connection with parameters provided
    struct ws_connection server_connection;
    server_connection.status = 0;
    server_connection.socket = socket(AF_INET,SOCK_STREAM,0);
    server_connection.port=Configuration::Instance().getWebPort();
    //Attempts to prevent the socket lock from occurring when shutting down.
    //See: https://stackoverflow.com/questions/24194961/how-do-i-use-setsockoptso-reuseaddr
    int sockopts[] = {1};
    if(setsockopt(server_connection.socket,SOL_SOCKET,SO_REUSEADDR,sockopts,sizeof(int)) < 0){
        server_connection.status=-1;
        return server_connection;
    }

    //Sets up the socket connection information. Binds and Listens on that port as well.
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(Configuration::Instance().getWebPort());
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(server_connection.socket, (struct sockaddr*) &server_address, sizeof(server_address)) < 0){
        server_connection.status = -1;
        BOOST_LOG_TRIVIAL(error) << strerror(errno);
//        perror("ERROR: Bind port");
        return server_connection;
    }

    if(listen(server_connection.socket,5) < 0) {
        server_connection.status = -1;
        BOOST_LOG_TRIVIAL(error) << strerror(errno);
//        perror("ERROR: Listen port");
        return server_connection;
    }
    return server_connection;
}

WebInterface::WebInterface(bool autostart) {
    runThread = autostart;
    if(autostart){
        Start();
    }
}

void WebInterface::Start(){
    runThread = true;
    if(!webthread.joinable()) {
        webthread = std::thread(&WebInterface::loop, this);
    }
}

void WebInterface::Stop(){
    runThread = false;
    if(webthread.joinable()){
        shutdown(currConnection.socket,SHUT_RDWR);
        webthread.join();
    }
}

WebInterface::~WebInterface() {
    Stop();
}
