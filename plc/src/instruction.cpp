//
// Created by paulgrahek on 3/11/19.
//

#include "instruction.h"

std::map<std::string,std::function<void(CPU&)>> Instruction::VoidInstructionStringFunctionMap;
std::map<std::string,std::function<void(CPU&,DataAddress)>> Instruction::SingleAddressInstructionStringFunctionMap;

void Instruction::init() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    /// Control Instructions
    VoidInstructionStringFunctionMap.emplace("BR",[](auto && cpu) { return cpu.BR(); });
    VoidInstructionStringFunctionMap.emplace("BN",[](auto && cpu) { return cpu.BN(); });
    VoidInstructionStringFunctionMap.emplace("BE",[](auto && cpu) { return cpu.BE(); });
    VoidInstructionStringFunctionMap.emplace("RNG",[](auto && cpu) { return cpu.RNG(); });
    /// Basic Instructions
    SingleAddressInstructionStringFunctionMap.emplace("XIC",[](auto && cpu, auto && addr) { return cpu.XIC(addr); });
    SingleAddressInstructionStringFunctionMap.emplace("XIO",[](auto && cpu, auto && addr) { return cpu.XIO(addr); });
    SingleAddressInstructionStringFunctionMap.emplace("OTE",[](auto && cpu, auto && addr) { return cpu.OTE(addr); });
    SingleAddressInstructionStringFunctionMap.emplace("OTL",[](auto && cpu, auto && addr) { return cpu.OTL(addr); });
    SingleAddressInstructionStringFunctionMap.emplace("OTU",[](auto && cpu, auto && addr) { return cpu.OTU(addr); });
    SingleAddressInstructionStringFunctionMap.emplace("OSR",[](auto && cpu, auto && addr) { return cpu.OSR(addr); });
}

void Instruction::run(CPU& cpu) {
    funct(cpu);
}
