//
// Created by paulp on 3/11/2019.
//

#include "console.h"
#include "plc.h"

void Console::Loop() {
    int run = 1;
    while(run){
        std::string input;
        getline(std::cin,input);
        if(input == "quit") break;
        else if(input =="reset"){
            PLC::Reset();
        }
        else if(input == "sb") {
            std::cout << "Enter Address: ";
            std::string addrStr;
            getline(std::cin,addrStr);
            DataAddress addr = DataAddress::parseDataAddressString(addrStr);
            std::cout << std::endl;
            std::cout << "Bit Value: " << std::endl;
            bool bitVal;
            std::cin >> bitVal;
            PLC::getCPU().setDataBit(addr,bitVal);
        }
    }
    PLC::Stop();

}
