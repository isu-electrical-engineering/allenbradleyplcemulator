//
// Created by paulp on 9/9/2019.
//

#include "controlfile.h"

bool ControlFile::setBit(DataAddress addr, bit val) {
    return false;
}

bit ControlFile::getBit(DataAddress addr) {
    return 0;
}

bool ControlFile::validateAddress(DataAddress addr) {
    return false;
}

word ControlFile::getWord(DataAddress addr) {
    return 0;
}

bool ControlFile::setWord(DataAddress addr, word val) {
    return false;
}
