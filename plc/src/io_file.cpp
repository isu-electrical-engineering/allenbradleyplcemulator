//
// Created by paulgrahek on 3/15/19.
//

#include "io_file.h"
#include "coutsync.h"

IOFile::IOFile(bool isInput) {
    type = (DataType)isInput;
    // Set all terminals as valid
    for(auto word : validTerminals){
        for(int term = 0; term < word.size(); ++term){
            word[term] = 1;
        }
    }
}

bool IOFile::setBit(DataAddress addr, bit val) {
    if(validateAddress(addr)){
        // Valid Address
        terminals[addr.elementNumber][addr.wordNumber][addr.bitNumber] = val;
        return true;
    }
    return false;
}

bit IOFile::getBit(DataAddress addr) {
    if(validateAddress(addr)){
        // Valid Address
        return terminals[addr.elementNumber][addr.wordNumber][addr.bitNumber];
    }
    else {
        // Invalid Address
        return 0;
    }
}

bool IOFile::validateAddress(DataAddress addr) {
    if(addr.type != type) {
        // Wrong Type
        return false;
    }
    else if(addr.fileNumber != type){
        // Bad Address: File Number
        syncCout << "ERROR: IO - Bad Address - File Number: " << addr.fileNumber << syncEndl;
        return false;
    }
    else if((IO_MinSlot > addr.elementNumber) || addr.elementNumber >= IO_SlotCount){
        // Bad Address: Slot Number
        syncCout << "ERROR: IO - Bad Address - Slot Number: " << addr.elementNumber << syncEndl;
        return false;
    }
    else if((IO_MinWord > addr.wordNumber) || (addr.elementNumber >= IO_WordCount)){
        // Bad Address: Word number
        syncCout << "ERROR: IO - Bad Address - Word Number: " << addr.wordNumber << syncEndl;
        return false;
    }
    else if((IO_MinTerminal > addr.bitNumber) || (addr.bitNumber >= IO_TerminalCount)){
        // Bad Address: Terminal number
        syncCout << "ERROR: IO - Bad Address - Terminal Number " << addr.bitNumber << syncEndl;
        return false;
    }
    else if(validTerminals[addr.elementNumber][addr.wordNumber][addr.bitNumber]){
        // Bad Address: Invalid Terminal
        syncCout << "ERROR: IO - Bad Address - Invalid Terminal "<< syncEndl;
        return false;
    }
    return true;
}

word IOFile::getWord(DataAddress addr) {
    if(validateAddress(addr)){
        // Valid Address
        return (word)terminals[addr.elementNumber][addr.wordNumber].to_ulong();
    }
    else {
        // Invalid Address
        return 0;
    }
}

bool IOFile::setWord(DataAddress addr, word val) {
    if(validateAddress(addr)){
        // Valid Address
        terminals[addr.elementNumber][addr.wordNumber] = std::bitset<16>(val);
        return true;
    }
    return false;
}


