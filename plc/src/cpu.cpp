//
// Created by paulgrahek on 8/21/19.
//

#include <interpreter.h>
#include <bitfile.h>
#include <plc.h>
#include <counterfile.h>
#include <timerfile.h>
#include <statusfile.h>
#include <integerfile.h>
#include <floatfile.h>
#include <controlfile.h>
#include "cpu.h"
#include "fault_codes.h"

void CPU::loop() {
    // Create a standard stop interval
    std::chrono::milliseconds stopInterval(10);
    //Primary running loop
    while(runThread){
        // Set the minimum time interval for the execution loop
        auto nextStop = std::chrono::system_clock::now() + stopInterval;
        // IO Functions
        write();
        read();
        // Ladder Logic
        ladderLogic();
        // Sleep until the minimum time interval is reached.
        if(!runThread) break;
        std::this_thread::sleep_until(nextStop);
    }
}

void CPU::write() {
    {
        std::unique_lock<std::shared_timed_mutex> lock(PLC::getOutputMutex());
        DataAddress outputFileAddr;
        outputFileAddr.type = Output;
        outputFileAddr.fileNumber = Output;
        for(uint16_t slot = IO_MinSlot; slot < IO_SlotCount; slot++){
            for(uint16_t word = IO_MinWord; word < IO_WordCount; word++){
                outputFileAddr.elementNumber = slot;
                outputFileAddr.wordNumber = word;
                PLC::getOutputBuffer().setRegister(slot, word, dataFiles[Output]->getWord(outputFileAddr));
            }
        }
    }
}

void CPU::read() {
    Interpreter::refreshProgramFiles(*this);
    {
        std::shared_lock<std::shared_timed_mutex> lock(PLC::getInputMutex());
        DataAddress inputFileAddr;
        inputFileAddr.type = Input;
        inputFileAddr.fileNumber = Input;
        for(uint16_t slot = IO_MinSlot; slot < IO_SlotCount; slot++){
            for(uint16_t word = IO_MinWord; word < IO_WordCount; word++){
                inputFileAddr.elementNumber = slot;
                inputFileAddr.wordNumber = word;
                dataFiles[Input]->setWord(inputFileAddr, PLC::getInputBuffer().getRegister(slot, word));
            }
        }
    }
}

void CPU::startup() {
    Instruction::init();
    resetProcessorFiles();
    DataAddress da = DataAddress::parseDataAddressString("I:1/0");
    for(uint8_t i = 0; i < 16; i +=2){
        da.bitNumber = i;
        dataFiles[da.fileNumber]->setBit(da,1);
    }
//    for(uint8_t i = 0; i < 16; i++){
//        da.bitNumber = i;
//        bit test = dataFiles[da.fileNumber]->getBit(da);
//        std::cout << test;
    std::cout.flush();
//    }

    Interpreter::readProgramFiles(*this,"./plcfiles/default/programfiles");
}

void CPU::ladderLogic() {
//    std::vector<int> previousBranchLevels{};
    stack.fill(0);
    rungNumber = -1;
    stackInd = 0;
    {
        ProgramFile& progFile = *programFiles[2];
        for(auto it = progFile.instructions.begin();it != progFile.instructions.end(); ++it){
            instructionNum++;
            (*it)->run(*this);
        }

    }
}

void CPU::resetProcessorFiles() {
    dataFiles[0] = std::make_unique<IOFile>(false);
    dataFiles[1] = std::make_unique<IOFile>(true);
    dataFiles[2] = std::make_unique<StatusFile>();
    dataFiles[3] = std::make_unique<BitFile>();
    dataFiles[4] = std::make_unique<TimerFile>();
    dataFiles[5] = std::make_unique<CounterFile>();
    dataFiles[6] = std::make_unique<ControlFile>();
    dataFiles[7] = std::make_unique<IntegerFile>();
    dataFiles[8] = std::make_unique<FloatFile>();
}

word CPU::getDataWord(DataAddress addr) {
    return dataFiles[addr.fileNumber]->getWord(addr);
}

bool CPU::setDataWord(DataAddress addr, word val) {
    if(dataFiles[addr.fileNumber]->validateAddress(addr)){
        dataFiles[addr.fileNumber]->setWord(addr,val);
        return true;
    }
    return false;
}

bit CPU::getDataBit(DataAddress addr) {
    return dataFiles[addr.fileNumber]->getBit(addr);
}

bool CPU::setDataBit(DataAddress addr, bit val) {
    if(dataFiles[addr.fileNumber]->validateAddress(addr)){
        dataFiles[addr.fileNumber]->setBit(addr,val);
        return true;
    }
    return false;
}

void CPU::run() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    startup();
    loop();
}

void CPU::Start() {
    runThread = true;
    if(!CPUThread.joinable()){
        CPUThread = std::thread(&CPU::run, this);
    }
}

void CPU::Stop() {
    runThread = false;
    if(CPUThread.joinable()){
        CPUThread.join();
    }
}

CPU::CPU(bool autostart) {
    resetProcessorFiles();
    runThread = autostart;
    if(autostart){
        Start();
    }
}

CPU::~CPU() {
    Stop();
}

int CPU::getFileNumber() const {
    return fileNumber;
}

int CPU::getRungNumber() const {
    return rungNumber;
}

int CPU::getNestLevel() const {
    return nestLevel;
}

int CPU::getBranchLevel() const {
    return branchLevel;
}

int CPU::getInstructionNum() const {
    return instructionNum;
}

int CPU::getStackInd() const {
    return stackInd;
}

void CPU::setFileNumber(int fileNumber) {
    CPU::fileNumber = fileNumber;
}

void CPU::setRungNumber(int rungNumber) {
    CPU::rungNumber = rungNumber;
}

void CPU::setNestLevel(int nestLevel) {
    CPU::nestLevel = nestLevel;
}

void CPU::setBranchLevel(int branchLevel) {
    CPU::branchLevel = branchLevel;
}

void CPU::setInstructionNum(int instructionNum) {
    CPU::instructionNum = instructionNum;
}

void CPU::setStackInd(int stackInd) {
    CPU::stackInd = stackInd;
}

const std::array<bit, 256> &CPU::getStack() const {
    return stack;
}

void CPU::setStack(const std::array<bit, 256> &stack) {
    CPU::stack = stack;
}

bit CPU::getStackValue(int stackInd) const {
    return stack[stackInd];
}

void CPU::setStackValue(int stackInd, bit value) {
    stack[stackInd] = value;
}

void CPU::set_fault_code(int faultCode) {
    this->faultCode = faultCode;
}
