//
// Created by paulgrahek on 3/15/19.
//

#include "bitfile.h"


bool BitFile::setWord(DataAddress addr, word val) {
    normalizeAddress(addr);
    // Ignore this?
    return false;
}

word BitFile::getWord(DataAddress addr) {
    normalizeAddress(addr);
    // Ignore this?
    return 0;
}

bool BitFile::validateAddress(DataAddress addr) {
    if(addr.type != type) {
        // Wrong Type
        return false;
    }
    else if(!(addr.fileNumber == 3 || addr.fileNumber > 8)){
        // Bad Address: File Number
        return false;
    }
    else if((BIT_MinWord > addr.elementNumber) || (addr.elementNumber >= BIT_WordCount)){
        // Bad Address: Slot Number
        return false;
    }
    else if((BIT_MinBit > addr.bitNumber) || (addr.bitNumber >= BIT_BitCount)){
        // Bad Address: Terminal number
        return false;
    }
    return true;
}

void BitFile::normalizeAddress(DataAddress &addr){
    if(addr.bitNumber > 15){
        addr.elementNumber = addr.bitNumber/16;
        addr.bitNumber = addr.bitNumber%16;
    }
}

bit BitFile::getBit(DataAddress addr) {
    normalizeAddress(addr);
    if(validateAddress(addr)){
        return bits[addr.elementNumber][addr.bitNumber];
    }
    return 0;
}

bool BitFile::setBit(DataAddress addr, bit val) {
    normalizeAddress(addr);
    if(validateAddress(addr)){
        bits[addr.elementNumber][addr.bitNumber] = val;
    }
    return false;
}
