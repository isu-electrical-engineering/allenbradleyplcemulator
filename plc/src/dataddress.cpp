//
// Created by paulp on 2/25/2019.
//

#include <vector>
#include <sstream>
#include "dataddress.h"
//
//void DataAddress::parseDataAddressString(std::string address) {
//
//}
DataAddress DataAddress::parseDataAddressString(std::string address) {
    DataAddress retAddr;
    if(address.empty()) return DataAddress();

    // Parse b
    std::string baseString = address;
    std::stringstream ss(baseString);
    std::string splitItem;
    std::vector<std::string> splittedStrings;
    while(std::getline(ss,splitItem,'/')){
        splittedStrings.push_back(splitItem);
    }
    if(splittedStrings.size() == 2){
        try{
            retAddr.bitNumber = std::stoi(splittedStrings[1]);
            baseString = splittedStrings[0];
        }
        catch (int e){
            // Error: bad parse
            return DataAddress();
        }
    }
    else if(splittedStrings.size() > 2){
        // Error: bad format
        return DataAddress();
    }

    // Parse s
    ss = std::stringstream(baseString);
    splitItem.clear();
    splittedStrings.clear();
    while(std::getline(ss,splitItem,'.')){
        splittedStrings.push_back(splitItem);
    }
    if(splittedStrings.size() == 2){
        try{
            retAddr.wordNumber = std::stoi(splittedStrings[1]);
            baseString = splittedStrings[0];
        }
        catch (int e){
            // Error: bad parse
            return DataAddress();
        }
    }
    else if(splittedStrings.size() > 2){
        // Error: bad format
        return DataAddress();
    }

    // Parse e
    ss = std::stringstream(baseString);
    splitItem.clear();
    splittedStrings.clear();
    while(std::getline(ss,splitItem,':')){
        splittedStrings.push_back(splitItem);
    }
    if(splittedStrings.size() == 2){
        try{
            retAddr.elementNumber = std::stoi(splittedStrings[1]);
            baseString = splittedStrings[0];
        }
        catch (int e){
            // Error: bad parse
            return DataAddress();
        }
    }
    else if(splittedStrings.size() > 2){
        // Error: bad format
        return DataAddress();
    }

    // Parse Data File Type
    int formatInd = 0;
    while(formatInd <= FloatingPoint){
        if(DataTypeChars[formatInd] == address[0]) break;
        formatInd++;
    }
    if(formatInd > FloatingPoint) return DataAddress();
    retAddr.type = (DataType)formatInd;
    // erase DataFileType indicator
    baseString.erase(0,1);

    // parse File Number
    if(baseString.empty()) retAddr.fileNumber = retAddr.type;
    else{
        try{
            retAddr.fileNumber = std::stoi(baseString);
        }
        catch (int e){
            // Error: bad parse
            return DataAddress();
        }
    }

    return retAddr;
}
