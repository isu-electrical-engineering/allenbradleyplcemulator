//
// Created by paulgrahek on 3/8/19.
//

#include "cpu.h"

/**
 * @brief Examine if Closed: Examines a bit for an On Condition
 *
 * Bit Address State | XIC Instruction
 * 0                 | False
 * 1                 | True
 *
 * @param addr
 * @author Paul Grahek
 */
void CPU::XIC(DataAddress addr) {
    stack[stackInd] &= getDataBit(addr);
}

/**
 * @brief Examine if Open: Examines a bit for an Off Condition
 *
 * Bit Address State | XIC Instruction
 * 0                 | True
 * 1                 | False
 *
 * @param addr
 * @author Paul Grahek
 */
void CPU::XIO(DataAddress addr) {
    stack[stackInd] &= !getDataBit(addr);
}

/**
 * @brief Output Energize: Sets an output to the value of the rung condition
 * Rung State | Address Value
 * 0          | False
 * 1          | True
 * @param addr
 * @author Paul Grahek
 */
void CPU::OTE(DataAddress addr) {
    setDataBit(addr,stack[stackInd]);
}

/**
 * @brief Output Latch: Retentive output - Can only turn on a bit
 * @param addr
 * @author Paul Grahek
 */
void CPU::OTL(DataAddress addr) {
    if(stack[stackInd]){
        setDataBit(addr,1);
    }
}

/**
 * @brief Output Unlatch: Retentive output - Can only turn off a bit
 * @param addr
 * @author Paul Grahek
 */
void CPU::OTU(DataAddress addr) {
    if(!stack[stackInd]){
        setDataBit(addr,0);
    }
}

/**
 * @brief One Shot Rising: Is True for One cycle, then false until the bit goes from 0 to 1
 * @param addr
 * @author Paul Grahek
 */
void CPU::OSR(DataAddress addr) {
    //TODO: test this, implement constraints regarding
    // which bits can be addressed per SLC 500 Instruction Set Reference Manual

    // Get the previous rung state, and then set the current rung state at the address.
    bit previousVal = getDataBit(addr);
    setDataBit(addr,stack[stackInd]);
    // If the OSR's last rung state was false and the new rung state is true,
    // put "true" on the stack, otherwise put "false"
    if(!previousVal
       && stack[stackInd]){
        stack[stackInd] = 1;
    }
    else{
        stack[stackInd] = 0;
    }
}


