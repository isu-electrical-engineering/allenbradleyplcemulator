//
// Created by paulgrahek on 4/10/19.
//

#include "io_buffer.h"

uint16_t IO_Buffer::getRegister(int slot, int word) {
    int offset = getRegisterOffset(slot, word);
    return ((terminals[offset] & ~forcedTerminals[offset]) |
            (forcedTerminals[offset] & forcedTerminalValues[offset])).to_ulong();
}

bit IO_Buffer::getTerminal(int slot, int word, int terminal) {
    int offset = getRegisterOffset(slot, word);
    return ((terminals[offset] & ~forcedTerminals[offset]) |
            (forcedTerminals[offset] & forcedTerminalValues[offset]))[terminal];
}

uint16_t IO_Buffer::getForcedRegister(int slot, int word) {
    int offset = getRegisterOffset(slot, word);
    return forcedTerminals[offset].to_ulong();
}

bit IO_Buffer::getForcedTerminal(int slot, int word, int terminal) {
    int offset = getRegisterOffset(slot, word);
    return forcedTerminals[offset][terminal];
}

void IO_Buffer::setRegister(int slot, int word, uint16_t reg) {
    int offset = getRegisterOffset(slot, word);
    terminals[offset] = reg;
}

void IO_Buffer::setTerminal(int slot, int word, int terminal, bit val) {
    int offset = getRegisterOffset(slot, word);
    terminals[offset][terminal] = val;
}

void IO_Buffer::setForcedRegister(int slot, int word, uint16_t reg) {
    int offset = getRegisterOffset(slot, word);
    forcedTerminals[offset] = ~0;
    forcedTerminalValues[offset] = reg;
}

void IO_Buffer::setForcedTerminal(int slot, int word, int terminal, bit val) {
    int offset = getRegisterOffset(slot, word);
    forcedTerminals[offset][terminal] = 1;
    forcedTerminalValues[offset][terminal] = val;
}

void IO_Buffer::unsetForcedRegister(int slot, int word) {
    int offset = getRegisterOffset(slot, word);
    forcedTerminals[offset] = 0;
}

void IO_Buffer::unsetForcedTerminal(int slot, int word, int terminal) {
    int offset = getRegisterOffset(slot, word);
    forcedTerminals[offset][terminal] = 0;
}
