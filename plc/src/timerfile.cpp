//
// Created by paulp on 4/22/2019.
//

#include "timerfile.h"
#include "coutsync.h"

bool TimerFile::setWord(DataAddress addr, word val) {
    if(validateAddress(addr)){
        timerData[addr.elementNumber][addr.wordNumber] = val;
        return true;
    }
    return false;
}

word TimerFile::getWord(DataAddress addr) {
    if(validateAddress(addr)){
        return timerData[addr.elementNumber][addr.wordNumber].to_ulong();
    }
    return 0;
}

bool TimerFile::validateAddress(DataAddress addr) {
    if(addr.type != type) {
        // Wrong Type
        syncCout << "ERROR: Timer - Bad Address - Type: " << addr.type << syncEndl;
        return false;
    }
    else if(!(addr.fileNumber == Timer || addr.fileNumber > 8)){
        // Bad Address: File Number
        syncCout << "ERROR: Timer - Bad Address - File Number: " << addr.fileNumber << syncEndl;
        return false;
    }
    else if((TIMER_MinElement > addr.elementNumber) || (addr.elementNumber >= TIMER_ElementCount)){
        // Bad Address: Slot Number
        syncCout << "ERROR: Timer - Bad Address - Element Number: " << addr.fileNumber << syncEndl;
        return false;
    }
    else if((TIMER_MinWord > addr.wordNumber) || (addr.wordNumber >= TIMER_WordCount)){
        // Bad Address: Slot Number
        syncCout << "ERROR: Timer - Bad Address - Word Number: " << addr.wordNumber << syncEndl;
        return false;
    }
    else if((TIMER_MinBit > addr.bitNumber) || (addr.bitNumber >= TIMER_BitCount)){
        // Bad Address: Terminal number
        syncCout << "ERROR: Timer - Bad Address - Bit Number: " << addr.bitNumber << syncEndl;
        return false;
    }
    return true;
}


bit TimerFile::getBit(DataAddress addr) {
    if(validateAddress(addr)){
        return timerData[addr.elementNumber][addr.wordNumber][addr.bitNumber];
    }
    return 0;
}

bool TimerFile::setBit(DataAddress addr, bit val) {
    if(validateAddress(addr)){
        timerData[addr.elementNumber][addr.wordNumber][addr.bitNumber] = val;
    }
    return false;
}