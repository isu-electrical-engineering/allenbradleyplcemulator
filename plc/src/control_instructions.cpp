//
// Created by paulgrahek on 3/8/19.
//

#include "plc.h"

/**
* @brief BR - Branch - Indicates the beginning of a branch
* @param plc
* @author Paul Grahek
*/
void CPU::BR() {
    // Increments Nest Level, Saves previous branch level and resets branch level to 0
    nestLevel++;
    previousBranchLevels.push_back(branchLevel);
    branchLevel = 0;

    // Increments stack Index to indicate a nested branch output and sets the value to 0
    stackInd+=1;
    stack[stackInd] = 0;

    // Increments the stack index to another level to process the branch individually
    stackInd+=1;
    stack[stackInd] =
            stack[stackInd-2];
}

/**
 * @brief BN - Branch Next - Indicates next level of branch
 *
 * @author Paul Grahek
 */
void CPU::BN() {
    // OR nested output with branch output
    stack[stackInd-1] |=
            stack[stackInd];

    // Increment Branch Level
    branchLevel++;

    //Resets input value for stack
    stack[stackInd] =
            stack[stackInd-2];
}

/**
 * @brief BE - Branch End - Indicates end of branch
 * @author Paul Grahek
 */
void CPU::BE() {
    // OR nested output with branch output
    stack[stackInd-1] |=
            stack[stackInd];

    // AND main rail output to branch output
    stack[stackInd-2] &=
            stack[stackInd-1];
    stackInd-=2;

    // Decrement Nest Level and set branch level to branch of previous level
    nestLevel--;
    branchLevel = previousBranchLevels.back();
    previousBranchLevels.pop_back();
}

/**
 * @brief RNG - Rung - Indicates new rung
 * @author Paul Grahek
 */
void CPU::RNG() {
    // Increment Rung Number
    rungNumber += 1;
    // Reset Nest level, Branch level, and Instruction number
    nestLevel = 0;
    branchLevel = 0;
    instructionNum = 0;
    // Clear the stack and reset the stack index
    stack.fill(0);
    stackInd = 0;
    // Set the first stack value to true
    stack[0] = true;

}