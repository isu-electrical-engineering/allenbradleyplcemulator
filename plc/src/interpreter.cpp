//
// Created by paulgrahek on 3/8/19.
//

#include "interpreter.h"
std::chrono::high_resolution_clock::time_point Interpreter::refreshTime = std::chrono::high_resolution_clock::now();

std::shared_ptr<Instruction> Interpreter::parseLine(const std::string &line) {
    std::istringstream buffer(line);
    std::string functionTok;
    buffer >> functionTok;
    std::shared_ptr<Instruction> instr = nullptr;
    std::vector<std::string> args((std::istream_iterator<std::string>(buffer)),
                                  std::istream_iterator<std::string>());
    if (Instruction::VoidInstructionStringFunctionMap.count(functionTok)){
        auto cpuInstr = Instruction::VoidInstructionStringFunctionMap.find(functionTok)->second;
        std::function<void(CPU&)> cpuInstrFunction = [cpuInstr](auto && cpu){ cpuInstr(cpu);};
        instr = std::make_shared<Instruction>(cpuInstrFunction);
    }
    else if(Instruction::SingleAddressInstructionStringFunctionMap.count(functionTok)){
        DataAddress addr = DataAddress::parseDataAddressString(args[0]);
        auto cpuInstr = Instruction::SingleAddressInstructionStringFunctionMap.find(functionTok)->second;
        std::function<void(CPU&)> cpuInstrFunction = [cpuInstr,addr](auto && cpu){ cpuInstr(cpu,addr);};
        instr = std::make_shared<Instruction>(cpuInstrFunction);
    }
    return instr;
}

std::shared_ptr<ProgramFile> Interpreter::readProgramFile(std::string fullFileName) {
    std::shared_ptr<ProgramFile> progFile = std::make_shared<ProgramFile>(ProgramFile());
    progFile->fullFileName = fullFileName;
    std::ifstream file(fullFileName);
    if(file.is_open()){
        std::string line;
        while(std::getline(file,line)){
            std::shared_ptr<Instruction> instr = parseLine(line);
            if(instr != nullptr){
                progFile->instructions.push_back(instr);
            }
        }
    }
    file.close();
    return progFile;
}

void Interpreter::readProgramFiles(CPU& cpu, std::string programFilesDirectory) {
    std::string fullFileName = programFilesDirectory;
    if(fullFileName.back() != '/') fullFileName += '/';
    for(int i = 2; i < MAX_PROGRAM_FILES; i++){
        cpu.programFiles[i] =
            Interpreter::readProgramFile(fullFileName + "ProgramFile" + std::to_string(i));
    }
}

void Interpreter::refreshProgramFiles(CPU& cpu) {
    // Refreshes program files every 5 seconds
    if(refreshTime < std::chrono::high_resolution_clock::now()) {
        for(size_t i = 0; i < cpu.programFiles.size(); i++){
            if(cpu.programFiles[i] == NULL) break;
            auto progFile = cpu.programFiles[i];
            struct stat fileStat;
            if(stat(progFile->fullFileName.c_str(), &fileStat) == 0){
                cpu.programFiles[i] = readProgramFile(progFile->fullFileName);
            }
        }
        refreshTime = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000);
    }

}

void Interpreter::refreshDataFiles(CPU& cpu) {
    std::string baseFileName = "plcfiles/plc1/datafiles";
    if(baseFileName.back() != '/') baseFileName += '/';
    baseFileName += "DataFile";
    // Refresh all data files from disk
    for(int i = 0; i < cpu.dataFiles.size(); i++){
//        PLC::getCPU().dataFiles.
    }
}
