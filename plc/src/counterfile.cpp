//
// Created by paulp on 4/22/2019.
//

#include "counterfile.h"
#include "coutsync.h"

bool CounterFile::setWord(DataAddress addr, word val) {
    if(validateAddress(addr)){
        counterData[addr.elementNumber][addr.wordNumber] = val;
        return true;
    }
    return false;
}

word CounterFile::getWord(DataAddress addr) {
    if(validateAddress(addr)){
        return counterData[addr.elementNumber][addr.wordNumber].to_ulong();
    }
    return 0;
}

bool CounterFile::validateAddress(DataAddress addr) {
    if(addr.type != type) {
        // Wrong Type
        syncCout << "ERROR: Counter - Bad Address - Type: " << addr.type << syncEndl;
        return false;
    }
    else if(!(addr.fileNumber == Counter || addr.fileNumber > 8)){
        // Bad Address: File Number
        syncCout << "ERROR: Counter - Bad Address - File Number: " << addr.fileNumber << syncEndl;
        return false;
    }
    else if((COUNTER_MinElement > addr.elementNumber) || (addr.elementNumber >= COUNTER_ElementCount)){
        // Bad Address: Slot Number
        syncCout << "ERROR: Counter - Bad Address - Element Number: " << addr.fileNumber << syncEndl;
        return false;
    }
    else if((COUNTER_MinWord > addr.wordNumber) || (addr.wordNumber >= COUNTER_WordCount)){
        // Bad Address: Slot Number
        syncCout << "ERROR: Counter - Bad Address - Word Number: " << addr.wordNumber << syncEndl;
        return false;
    }
    else if((COUNTER_MinBit > addr.bitNumber) || (addr.bitNumber >= COUNTER_BitCount)){
        // Bad Address: Terminal number
        syncCout << "ERROR: Counter - Bad Address - Bit Number: " << addr.bitNumber << syncEndl;
        return false;
    }
    return true;
}


bit CounterFile::getBit(DataAddress addr) {
    if(validateAddress(addr)){
        return counterData[addr.elementNumber][addr.wordNumber][addr.bitNumber];
    }
    return 0;
}

bool CounterFile::setBit(DataAddress addr, bit val) {
    if(validateAddress(addr)){
        counterData[addr.elementNumber][addr.wordNumber][addr.bitNumber] = val;
    }
    return false;
}