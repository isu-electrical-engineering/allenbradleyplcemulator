#include <iostream>
#include <functional>
#include "plc.h"
#include <map>
#include <cstring>
#include "console.h"
#include "webinterface.h"
#include "config.h"

void ParseCommandArgs(int argc, char* argv[]);
bool IsReservedToken(char* arg);

int main(int argc, char** argv) {
    ParseCommandArgs(argc,argv);
    PLC::SetUp(true);
    Console::Loop();
    return 0;
}

void ParseCommandArgs(int argc, char** argv){
    for(int arg = 1; arg < argc; arg++){
        if(IsReservedToken(argv[arg])){
           if(strcmp(argv[arg],"--config-file")==0){
               if(arg + 1 < argc){
                   arg++;
                   Configuration::Instance().ReloadConfig(argv[arg]);
               }
           }
        }
    }
}

bool IsReservedToken(char* arg){
    if(strcmp(arg,"--config-file") == 0){
        return true;
    }
    return false;
}