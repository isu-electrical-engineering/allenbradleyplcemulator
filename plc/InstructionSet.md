# Supported Instructions for Allen-Bradley Emulator

## Basic Instructions
- XIC
- XIO
- OTE
- OTL
- OTU
- OSR (IN PROGRESS)

## Control Instructions
- RNG
- BR
- BN
- BE