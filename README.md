# Setup and Execution
Open a terminal, and run the following commands to start the emulator
``` bash
# For Fedora
sh ./setup-fedora.sh
# For Ubuntu
sh ./setup-ubuntu.sh

# For both
sh ./run.sh 
```