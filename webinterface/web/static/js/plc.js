const slotCount = 31;
const wordCount = 256;
const terminalCount = 16;

/*
    Connector for SocketIO
 */
var socket = io.connect('http://' + document.domain + ':' + location.port);

/*
    SocketIO connection after page load. Asks for PLC data on connect
 */
socket.on('connect',function(){
   socket.emit('getPLCData');
});

/*
   SocketIO connection to receive data from the PLC. Updates terminals accordingly
   TODO: Account for Analog Inputs
 */
socket.on('getPLCData',function(plcdata){
    let terminalsUpdated = 0;
    // For each output type in the data
    for(let outputType in plcdata){
        // Iterate through each slot
        for(let slot = 1; slot < slotCount; ++slot){
            // Iterate through each word
            for(let word = 0; word < wordCount; ++word){
                // Iterate through each terminal and update display accordingly
                for(let terminal = 0; terminal < terminalCount; ++terminal){
                    let term = document.getElementById(outputType + "-" + slot + "-" + word + "-" + terminal);
                    if(term){
                        let val = ((plcdata[outputType][slot][word]["Data"] >>> terminal) & 1).toString();
                        terminalsUpdated++;
                        // If the terminal is not locked, update the value
                        if(!term.classList.contains("lockedTerminal")){
                            term.value = val;
                            if(((plcdata[outputType][slot][word]["Forced"] >>> terminal) & 1) === 0) {
                                if(val === "1"){
                                    term.classList.add("activeTerminal");
                                }
                                else{
                                    term.classList.remove("activeTerminal");
                                }
                                term.classList.remove("forcedTerminal")
                            }
                            else{
                                term.classList.remove("activeTerminal");
                                term.classList.add("forcedTerminal");
                            }
                        }

                    }

                }
            }
        }
    }
    console.log("Terminals Updated: " + terminalsUpdated);
});

function hideRow(){

}

/*
    Locks a terminal when a user enters it to edit
 */
function lockTerminal(term){
    if(term){
        term.originalValue=term.value;
        term.classList.add("lockedTerminal");
        term.isEdited=false;
        term.select();
    }
}

/*
    Ensures forced terminal input is a correct value for a single bit
 */
function forceTerminalInput(term){
    if(term){
        term.isEdited=true;
        if(term.value) {
            if(term.value >= 1) term.value = 1;
            else term.value = 0;
        }
    }
}

/*
    Unlocks a terminal when the user submits their value
 */
function unlockTerminal(term){
    if(term){
        var val = term.value;
        if(!val || !term.isEdited){
            term.value = term.originalValue;
        }
        if(term.isEdited) {
            var idData = term.id.split("-");
            var dataType = idData[0]==="Output"?"Output":"Input";
            var setData = {
                "Set": {
                    "Type" : dataType,
                    "Slot" : idData[1],
                    "Word" : idData[2],
                    "Terminal" : idData[3],
                    "Value" : val
                }
            };
            socket.emit('setPLCData',setData)
            term.classList.add("forcedTerminal");
        }
        term.classList.remove("lockedTerminal");

        term.isEdited = false;
}
}

function exitOnEnter(term){
    if(event.keyCode===13){

    }
}